#include "NetPackets.h"
#include "usefulfuncs.h"

ENetHost * host;
ENetPeer * serverpeer;
uint8_t tempnetbuf[NETBUF_LENGTH];

bool isServer;
bool serverquit;

// Sent by server - Server Info
void Net_SendServerInfo(ENetPeer* client)
{
	if (!isServer)
		return;

	int i, l;

	tempnetbuf[0] = PACKET_SERVERINFO;
	tempnetbuf[1] = lobby.partySize;
	tempnetbuf[2] = lobby.partyMax;
	l = 3;

	for (i = 0; clientname[i]; i++)
	{
		tempnetbuf[l++] = clientname[i];
	}
	tempnetbuf[l++] = '|';

	for (i = 0; lobby.modname[i]; i++)
	{
		tempnetbuf[l++] = lobby.modname[i];
	}
	tempnetbuf[l++] = 0;

	enet_peer_send(client, CHAN_PRIMARY, enet_packet_create(&tempnetbuf[0], l, ENET_PACKET_FLAG_RELIABLE));
}

// Recieved by clients - Server Info
void Net_RecieveServerInfo(uint8_t* pbuf, int32_t packbufleng)
{
	if (isServer)
		return;

	int i = 1;
	int j = 0;
	char server_name[128];

	pbuf[0] = 'S';
	lobby.partySize = myPlayerIndex = pbuf[i++] + 1;
	lobby.partyMax = pbuf[i++];

	// Fill buffer until we hit a pipe.
	for (; pbuf[i] != '|'; i++)
	{
		server_name[j] = pbuf[i];
		j++;
	}
	server_name[j] = 0; // Terminate string

	i++; // Skip past the pipe
	j = 0;
	for (; pbuf[i]; i++)
	{
		lobby.modname[j] = pbuf[i];
		j++;
	}
	lobby.modname[j] = 0; // Terminate string

	if (lobby.modname[0] == 0)
	{
		colorprintf("Got response from server! Host: %s " C_RESET "(Players: %d/%d)\n\n", server_name, lobby.partySize, lobby.partyMax);
	}
	else
	{
		colorprintf("Got response from server! Host: %s " C_RESET "- Mod: %s (Players: %d/%d)\n\n", server_name, lobby.modname, lobby.partySize, lobby.partyMax);
	}

	printf("Lobby is waiting, please wait patiently for the host to launch...\n");
}

// Sent by server - Start Game
void Net_StartGame(void)
{
	if (!isServer)
		return;

	printf("Sending StartGame packets...\n");

	tempnetbuf[0] = PACKET_GAMESTART;
	enet_host_broadcast(host, CHAN_PRIMARY, enet_packet_create(&tempnetbuf[0], 1, ENET_PACKET_FLAG_RELIABLE));

	ENetEvent event;
	while (enet_host_service(host, &event, 0) > 0)
	{
		switch (event.type)
		{
		case ENET_EVENT_TYPE_RECEIVE:
			enet_packet_destroy(event.packet);
			break;
		}
	}
}

// Sent by clients - Client info
void Net_SendClientInfo(ENetPeer* client)
{
	if (isServer)
		return;

	int i, l;

	tempnetbuf[0] = PACKET_CLIENTINFO;
	l = 1;

	for (i = 0; clientname[i]; i++)
	{
		tempnetbuf[l++] = clientname[i];
	}
	tempnetbuf[l++] = 0;

	enet_peer_send(client, CHAN_CLIENTINFO, enet_packet_create(&tempnetbuf[0], l, ENET_PACKET_FLAG_RELIABLE));
}

// Recieved by server - Client Info
void Net_RecieveClientInfo(uint8_t* pbuf, int32_t packbufleng, ENetEvent* event)
{
	if (!isServer)
		return;

	int i = 1;
	int j = 0;
	char client_name[128];

	lobby.partySize++;

	for (; pbuf[i]; i++)
	{
		client_name[j] = pbuf[i];
		j++;
	}
	client_name[j] = 0;

	if (isServer)
	{
		event->peer->data = malloc(sizeof(client_name));
		snprintf(reinterpret_cast<char*>(event->peer->data), sizeof(client_name), "%s", client_name);
		//memcpy(event->peer->data, client_name, sizeof(client_name));
	}

	colorprintf("%s " C_RESET "connected! [%d/%d]\n", client_name, lobby.partySize, lobby.partyMax);
}

void Net_ParseClientPacket(ENetEvent* event)
{
	uint8_t* pbuf = event->packet->data;
	int32_t packbufleng = event->packet->dataLength;

	switch (pbuf[0])
	{
	case PACKET_CLIENTINFO:
		Net_RecieveClientInfo(pbuf, packbufleng, event);
		break;
	}
}

void Net_ParseServerPacket(ENetEvent* event)
{
	uint8_t* pbuf = event->packet->data;
	int32_t packbufleng = event->packet->dataLength;

	switch (pbuf[0])
	{
	case PACKET_SERVERINFO:
		Net_RecieveServerInfo(pbuf, packbufleng);
		break;
	case PACKET_CLIENTINFO:
		Net_RecieveClientInfo(pbuf, packbufleng, NULL);
		break;
	case PACKET_GAMESTART:
		lobby.starting = true;
		break;
	}
}

void Net_HandlePackets(void)
{
	ENetEvent event;
	enet_host_service(host, NULL, 0);

	while (enet_host_check_events(host, &event) > 0)
	{
		switch (event.type)
		{
		case ENET_EVENT_TYPE_CONNECT:
			char ip[16];
			enet_address_get_host_ip(&event.peer->address, ip, sizeof(ip));

			if (isServer)
			{
				printf("A new client connecting from %s:%u.\n", ip, event.peer->address.port);
				event.peer->data = const_cast<char*>("Unknown client");
				event.peer->pingInterval = 250;
				event.peer->timeoutLimit = ENET_PEER_TIMEOUT_LIMIT;
				event.peer->timeoutMinimum = ENET_PEER_TIMEOUT_MINIMUM;
				event.peer->timeoutMaximum = ENET_PEER_TIMEOUT_MAXIMUM;
				Net_SendServerInfo(event.peer);
			}
			else
			{
				printf("Connected to %s:%u.\n\n", ip, event.peer->address.port);
				event.peer->data = const_cast<char*>("Server");
				Net_SendClientInfo(event.peer);
			}
			break;

		case ENET_EVENT_TYPE_RECEIVE:
			if (isServer)
			{
				Net_ParseClientPacket(&event);

				if (event.channelID == CHAN_CLIENTINFO) // If client info, broadcast to everyone.
				{
					const ENetPacket* pak = event.packet;
					event.peer->state = ENET_PEER_STATE_DISCONNECTED;
					enet_host_broadcast(host, event.channelID,
						enet_packet_create(pak->data, pak->dataLength, pak->flags & ENET_PACKET_FLAG_RELIABLE));
					event.peer->state = ENET_PEER_STATE_CONNECTED;
				}
			}
			else
			{
				Net_ParseServerPacket(&event);
			}

			/*
			int type = event.packet->data[0];
			event.packet->data[0] = 'P';
			printf("A packet type %d of length %lu containing %s was received from %s on channel %u.\n",
				type,
				event.packet->dataLength,
				static_cast<uint8_t*>(event.packet->data),
				static_cast<char*>(event.peer->data),
				event.channelID);
			*/

			enet_packet_destroy(event.packet);
			break;


		case ENET_EVENT_TYPE_DISCONNECT:
			if (isServer)
			{
				lobby.partySize--;
				colorprintf("%s " C_RESET "disconnected. [%d/%d]\n", static_cast<char*>(event.peer->data), lobby.partySize, lobby.partyMax);
				event.peer->data = NULL;
			}
			else
			{
				printf("Server disconnected. Exiting.\n");
				serverquit = true;
				killme(0);
			}
		}
	}
}

ENetHost* Net_CreateHost(size_t maxclients)
{
	ENetAddress address;
	address.host = ENET_HOST_ANY;
	address.port = GAMEPORT;

	host = enet_host_create(&address, maxclients, 2, 0, 0);

	if (host == NULL)
	{
		printf("An error occurred while trying to create an ENet server host.\n");
		return NULL;
	}

	return host;
}

ENetHost* Net_ConnectToHost(const char* ip)
{
	ENetAddress address;

	enet_address_set_host(&address, ip);
	address.port = GAMEPORT;

	host = enet_host_create(NULL, 1, 2, 0, 0);
	if (host == NULL)
	{
		printf("An error occurred while trying to create an ENet client.\n");
		return NULL;
	}
	else
	{
		printf("ENet client initialization successful!\n");
	}

	printf("Attempting to connect to host...\n");
	serverpeer = enet_host_connect(host, &address, 2, 0);
	if (serverpeer == NULL)
	{
		printf("Connection Failed - No available peers for initiating an ENet connection.\n");
		return NULL;
	}

	return host;
}

void Net_DestroyHost(void)
{
	if (host != NULL)
	{
		enet_host_destroy(host);
	}
}

// Sent disconnect packets on quit and cleanly de-init ENet.
void Net_Kill(void)
{
	if (!isServer && !serverquit)
	{
		if (serverpeer != NULL)
		{
			printf("Sending disconnect packet.\n");
			ENetEvent event;
			enet_peer_disconnect(serverpeer, 0);
			while (enet_host_service(host, &event, 1000) > 0)
			{
				switch (event.type)
				{
				case ENET_EVENT_TYPE_RECEIVE:
					enet_packet_destroy(event.packet);
					break;
				case ENET_EVENT_TYPE_DISCONNECT:
					printf("Disconnection from server succeeded.");
					break;
				}
			}
		}
	}
	else if (isServer)
	{
		if (host != NULL)
		{
			for (int i = 0; i < MAXCLIENTS; i++)
			{
				if (host == NULL || host->peers == NULL)
					break;

				ENetPeer* peer = &host->peers[i];

				if (peer == NULL)
					continue;

				if ((peer->state != ENET_PEER_STATE_DISCONNECTED) && (peer->state != ENET_PEER_STATE_ZOMBIE))
				{
					ENetEvent event;
					enet_peer_disconnect(peer, 0);

					while (enet_host_service(host, &event, 0) > 0)
					{
						switch (event.type)
						{
						case ENET_EVENT_TYPE_RECEIVE:
							enet_packet_destroy(event.packet);
							break;
						case ENET_EVENT_TYPE_DISCONNECT:
							printf("Disconnection of client succeeded.");
							break;
						}
					}
				}
			}
		}
	}

	enet_deinitialize();
}