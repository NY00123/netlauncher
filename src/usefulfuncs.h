#ifndef _USEFULFUNCS_
#define _USEFULFUNCS_

char* GetThisPath(char* dest, size_t destSize);

char* strremove(char* str, const char* sub);
char* extract_between(const char* str, const char* p1, const char* p2);

int prompt(char* line, size_t size);
void find_string(char * string, const char * pattern, char buffer[], int iBuffer, char c);
bool isValidIpAddress(char *ip);
void strip_newline(char *str);

const char* convertcolorcodes(char* out, const char* in);

#ifdef _WIN32
void colorprintf(const char *, ...);
char* convert(unsigned int, int);
#define kbhit _kbhit
#define getch _getch
#else
#define colorprintf printf
bool kbhit();
char getch();
#endif

#endif