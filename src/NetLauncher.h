#ifndef DISCORDLAUNCHER_H
#define DISCORDLAUNCHER_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h> // exit
#include <string.h>
#include <time.h>

#include <enet/enet.h>

#ifdef _WIN32
#include <windows.h>
#include <conio.h>
#include <signal.h>
#include <Shlwapi.h>
#include <direct.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <cstdio>
#include <netdb.h>
#include <limits.h>
#include <unistd.h> // readlink
#include <fcntl.h>
#include <termios.h>
#include <sys/wait.h> // waitpid
#endif

#include "crossplat.h"

#define APPLICATION_ID "381610007474733076";
#define APPLICATION_NAME "NetLauncher for EDuke32-OldMP"
#define APPLICATION_SHORTNAME "NetLauncher"

#define EDUKE32_CONFIG "./eduke32-oldmp.cfg"

#define GAMEPORT 23513
#define MAX_MODS_COUNT 32
#define MAXCLIENTS 16

#ifdef _WIN32
#define EXE_PATH "start eduke32-oldmp.exe"
#else
#define EXE_PATH "./eduke32-oldmp"
#endif

typedef struct LobbyData {
	int partySize;
	int partyMax;
	int fraglimit;
	bool starting;
	char modname[MAX_PATH];
} LobbyData;

void killme(int signal);

static void PrintBanner(void)
{
	printf("-----------------------------------\n"
           "NETCOMMIT Device Driver Version 1.3\n"
		   "-----------------------------------\n"
	);
}

extern char clientname[128];
extern int myPlayerIndex;
extern LobbyData lobby;

#endif