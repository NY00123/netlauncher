#ifndef NETPACKETS_H
#define NETPACKETS_H

#include "NetLauncher.h"

#define NETBUF_LENGTH 8192

enum NetPacket_t
{
	PACKET_SERVERINFO,
	PACKET_CLIENTINFO,
	PACKET_GAMESTART,
	PACKET_MESSAGE,
};

enum NetChan_t
{
	CHAN_PRIMARY,
	CHAN_CLIENTINFO,
	CHAN_MESSAGE,
};

void Net_HandlePackets(void);
void Net_StartGame(void);

ENetHost* Net_CreateHost(size_t maxclients);
ENetHost* Net_ConnectToHost(const char* ip);

void Net_DestroyHost(void);
void Net_Kill(void);

extern ENetHost* host;
extern ENetPeer* serverpeer;
extern uint8_t tempnetbuf[NETBUF_LENGTH];

extern bool isServer;
extern bool serverquit;

#endif