#ifdef _WIN32
	#ifndef _CRT_SECURE_NO_WARNINGS
		#define _CRT_SECURE_NO_WARNINGS /* thanks Microsoft */
	#endif
	#ifndef _WINSOCK_DEPRECATED_NO_WARNINGS
		#define _WINSOCK_DEPRECATED_NO_WARNINGS
	#endif
	#define WIN32_LEAN_AND_MEAN
#endif

#include "NetLauncher.h"
#include "NetPackets.h"

#include "usefulfuncs.h"
#include "ipify.h"
#include "profport.h"
#include "Router.h"

#ifdef _WIN32
#include "WinDump.h"
#endif

// Required Libraries
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "ws2_32.lib") 
#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "Iphlpapi.lib")
#pragma comment(lib, "winmm.lib")

//bool lanmode = false;

// ************
// MAIN PROGRAM
// ************
char clientname[128];

char path[MAX_PATH];

char modList[MAX_MODS_COUNT][MAX_PATH];
int num_mods;

int myPlayerIndex;

LobbyData lobby;

Router* router;

#ifdef _WIN32
PROCESS_INFORMATION procinfo;
STARTUPINFO info = { sizeof(info) };
#endif

void killme(int signal)
{
	Net_Kill();

	exit(0);
}

void PopulateModsList()
{
	char modPath[MAX_PATH];
	sprintf(modPath, "%s/mods", path);

	tinydir_dir dir;
	if (tinydir_open(&dir, modPath) != 0)
	{
		printf("Couldn't open mods dir.\n");
		return;
	}

	if (!dir.has_next)
	{
		printf("No mod files found.\n");
		return;
	}

	do
	{
		tinydir_file file;
		tinydir_readfile(&dir, &file);

		if (!file.is_dir &&
		    (!strcasecmp(file.extension, "grp") || !strcasecmp(file.extension, "pk3"))
		)
		{
			num_mods++;
			sprintf(modList[num_mods - 1], "%s", file.name);
		}
		tinydir_next(&dir);
	} while (dir.has_next && (num_mods < MAX_MODS_COUNT));

	tinydir_close(&dir);
}

void AskMod()
{
	char count[4];

	PopulateModsList();

	if (!num_mods)
		return;

	for (int i = 0; i < num_mods; i++)
	{
		printf("%d. %s\n", i + 1, modList[i]);
	}

	printf("Which Mod? (1-%d, 0 for None)", num_mods);
	while (1)
	{
		if (!prompt(count, sizeof(count)))
		{
			continue;
		}


		if (atoi(count) >= 1 && atoi(count) <= num_mods)
		{
			int selected_mod = atoi(count);
			sprintf(lobby.modname, "%s", modList[selected_mod - 1]);
			printf("Mod set to %s.\n", lobby.modname);
			break;
		}
		if (atoi(count) == 0)
		{
			printf("No mods.\n");
			break;
		}
		else
		{
			printf("Invalid selection!\n");
			continue;
		}
	}
}

void AskPlayerCount()
{
	char count[4];

	printf("Max Players? (2-16)");
	while (1)
	{
		if (!prompt(count, sizeof(count)))
		{
			continue;
		}

		if (atoi(count) >= 2 && atoi(count) <= 16)
		{
			lobby.partyMax = atoi(count);
			printf("Player count set to %d.\n", lobby.partyMax);
			break;
		}
		else
		{
			printf("Invalid player count!\n");
			continue;
		}
	}
}

void AskFraglimit()
{
	char count[4];

	printf("Frag limit? (0-100)");
	while (1)
	{
		if (!prompt(count, sizeof(count)))
		{
			continue;
		}

		if (atoi(count) >= 1 && atoi(count) <= 100)
		{
			lobby.fraglimit = atoi(count);
			printf("Frag limit set to %d.\n", lobby.fraglimit);
			break;
		}
		else if (atoi(count) == 0)
		{
			lobby.fraglimit = 0;
			printf("No frag limit.\n");
			break;
		}
		else
		{
			printf("Invalid!\n");
			continue;
		}
	}
}

int initRouter()
{
	printf("Initializing UPnP...\n");
	
	try
	{
		router = new Router(4000);
	}
	catch (std::runtime_error& ex)
	{
		printf("*** ERROR: %s\n", ex.what());
		Sleep(1000);
		return 0;
	}

	try
	{
		printf("Attempting UPnP Port Forward...\n");
		if (!router->setPortMapping(GAMEPORT, GAMEPORT, Router::MAP_UDP, "EDuke32"))
		{
			printf("Could not forward port.\n");
			Sleep(1000);
			return 0;
		}
	}
	catch (std::runtime_error& ex)
	{
		printf("*** ERROR: %s\n", ex.what());
		Sleep(1000);
		return 0;
	}

	return 1;
}

int router_initialized = 0;
void AskUPnP()
{
	char yn[4];
	printf("Attempt to forward ports with UPnP? (y/n)\nNOTE: This may, or may not work depending on your network setup!");
	while (1)
	{
		if (!prompt(yn, sizeof(yn)))
		{
			continue;
		}

		if (yn[0] == 'y')
		{
			router_initialized = initRouter();
			break;
		}

		if (yn[0] == 'n')
		{
			router_initialized = 0;
			break;
		}
	}
}

void StartGame(char *cmd_buffer)
{
	Net_DestroyHost();

#ifdef _WIN32
	system(cmd_buffer);

	/*
	int result = CreateProcess(EXE_PATH, cmd_buffer, NULL, NULL, FALSE, 0, NULL, NULL, &info, &procinfo);
	if (!result)
	{
		printf("Failed to launch EDuke32-OldMP!\n");
		return;
	}
	*/
#else
	if (access(EXE_PATH, X_OK) != 0)
	{
		printf("Can't find or execute %s! (access returned errno==%d)\n", EXE_PATH, errno);
		return;
	}

	int result = fork();
	if (result == 0) // child
	{
		// More-or-less what system() does (http://man7.org/linux/man-pages/man3/system.3.html)
		execl("/bin/sh", "sh", "-c", cmd_buffer, NULL);
		killme(0);
	}
#endif
}

void LaunchAndWait()
{
	char cmd_buffer[8192];
	char ip_addr[16];
	
	if (router_initialized)
	{
		std::string ip = router->getWanAddress();
		snprintf(ip_addr, sizeof(ip_addr), "%s", ip.c_str());
	}
	else
	{
		get_ip(ip_addr);
	}

	CLEAR_SCREEN;

	PrintBanner();

	if (router_initialized)
	{
		printf("UPnP: Enabled!\n");
		printf("Internal IP: %s\n", router->getLocalAddress().c_str());
	}
	else
	{
		char hostname[128];
		gethostname(hostname, sizeof(hostname));
		hostent *hp = gethostbyname(hostname);
		in_addr **addr;
		
		addr = (in_addr **)hp->h_addr_list;

		printf("UPnP: N/A - Might need to forward UDP port %d\n\n", GAMEPORT);
		printf("-=Internal IP List=-\n");
		while (*addr != NULL) {
			printf("%s\n", inet_ntoa(**(addr++)));
		}
		printf("\n");
	}

	if (ip_addr[0] == 0)
	{
		printf("External IP: Unknown! Offline?\n");
	}
	else
	{
		printf("External IP: %s\n", ip_addr);
	}

	if (lobby.modname[0] != 0)
	{
		printf("Mod: %s\n", lobby.modname);
	}
	else
	{
		printf("Mod: None.\n");
	}

	printf("Max Players: %d\n", lobby.partyMax);
	printf("Frag Limit: %d\n", lobby.fraglimit);

	printf("\n");

	if(Net_CreateHost(lobby.partyMax) == NULL)
	{
		killme(0);
	}
	else
	{
		printf("Lobby creation successful!\n");
	}

	isServer = true;

	printf("Waiting for players, press Y to launch early (needs at least 2 players)...\n\n");

	while (!lobby.starting)
	{
		Net_HandlePackets();

		if ((lobby.partySize > 1 && (kbhit() && getch() == 'y')) || (lobby.partySize == lobby.partyMax))
		{
			lobby.starting = true;
		}
		else if (kbhit() && getch() == 'q')
		{
			printf("Q pressed. Quitting.\n");
			killme(0);
		}

		Sleep(10);
	}

	printf("Starting game.\n");
	if (lobby.modname[0] != 0)
	{
		sprintf(cmd_buffer, EXE_PATH " -nologo -nosetup -noinstancechecking -g mods/%s -map E1L1.MAP -t23 -m -c1 -y%d -net -n0:%d", lobby.modname, lobby.fraglimit, lobby.partySize);
	}
	else
	{
		sprintf(cmd_buffer, EXE_PATH " -nologo -nosetup -noinstancechecking -map E1L1.MAP -t23 -m -c1 -y%d -net -n0:%d", lobby.fraglimit, lobby.partySize);
	}
	Net_StartGame();

	StartGame(cmd_buffer);
}

void ConnectToHost(const char *ip)
{
	char buffer[8192];
	isServer = false;

	CLEAR_SCREEN;
	PrintBanner();

	printf("Host IP: %s\n\n", ip);

	Net_ConnectToHost(ip);

	while (!lobby.starting)
	{
		Net_HandlePackets();
		Sleep(10);
	}

	printf("Starting game.\n");

	if (lobby.modname[0] != 0)
	{
		sprintf(buffer, EXE_PATH " -nologo -nosetup -noinstancechecking -map E1L1.MAP -t23 -m -c1 -g mods/%s -net -n0 %s -p%d", lobby.modname, ip, GAMEPORT + myPlayerIndex);
	}
	else
	{
		sprintf(buffer, EXE_PATH " -nologo -nosetup -noinstancechecking -map E1L1.MAP -t23 -m -c1 -net -n0 %s -p%d", ip, GAMEPORT + myPlayerIndex);
	}

	StartGame(buffer);

	killme(0);
}

void AskManualIP(bool forced)
{
	char buffer[8192];
	char ip[16];
	printf("Enter IP Address:\n");
	
	fgets(ip, 16, stdin);
	ip[strcspn(ip, "\n")] = 0;

	if (isValidIpAddress(ip))
	{
		if(forced)
		{ 
			if (lobby.modname[0] != 0)
			{
				sprintf(buffer, EXE_PATH " -nologo -nosetup -noinstancechecking -map E1L1.MAP -t23 -m -c1 -g mods/%s -net -n0 %s -p%d", lobby.modname, ip, GAMEPORT);
			}
			else
			{
				sprintf(buffer, EXE_PATH " -nologo -nosetup -noinstancechecking -map E1L1.MAP -t23 -m -c1 -net -n0 %s -p%d", ip, GAMEPORT);
			}

			StartGame(buffer);
			killme(0);
		}
		else
		{
			ConnectToHost(ip);
		}
	}
	else
	{
		printf("Invalid IP!\n\n");
		AskManualIP(forced);
	}
}

void AskIfHostingOrJoining()
{
	char yn[4];
	printf("Select one:\n\n");

	colorprintf(C_BOLD C_YELLOW "1. " C_RESET "Host Game\n");
	colorprintf(C_BOLD C_YELLOW "2. " C_RESET "Join via IP\n");
	colorprintf(C_BOLD C_YELLOW "3. " C_RESET "Force EDuke32-OldMP Connection (For Troubleshooting)\n");
	colorprintf(C_BOLD C_RED "4. " C_RESET "Quit\n");

	while (1)
	{
		if (!prompt(yn, sizeof(yn)))
		{
			continue;
		}

		if (yn[0] == '1')
		{
			printf("\n");
			AskPlayerCount();
			printf("\n");
			AskFraglimit();
			printf("\n");
			AskMod();
			printf("\n");
			AskUPnP();
			LaunchAndWait();
			killme(0);
		}

		if (yn[0] == '2')
		{
			AskManualIP(false);
		}

		if (yn[0] == '3')
		{
			printf("NOTE: Make sure to select the same mod as the host!\n");
			AskMod();
			AskManualIP(true);
		}

		if (yn[0] == '4')
		{
			killme(0);
		}
	}
}

typedef void(*dllSetString)(const char*);

int main(int argc, char** argv)
{
	char* ip;
	bool start_with_ip = false;

	GetThisPath(path, MAX_PATH);
	colorprintf("Current Directory: " C_BOLD C_CYAN "%s\n" C_RESET, path);
	chdir(path);

	// FIXME - Alternative to SetConsoleTitle for Linux?
#ifdef _WIN32
	SetUnhandledExceptionFilter(unhandled_handler);
	SetConsoleTitle(APPLICATION_NAME);
/*
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	DWORD dwMode = 0;
	GetConsoleMode(hOut, &dwMode);
	dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
	SetConsoleMode(hOut, dwMode);
*/

#ifdef _DEBUG
	HMODULE ebacktrace = LoadLibraryA("ebacktrace1.dll");
	if (ebacktrace)
	{
		dllSetString SetTechnicalName = (dllSetString)(void(*)(void))GetProcAddress(ebacktrace, "SetTechnicalName");
		dllSetString SetProperName = (dllSetString)(void(*)(void))GetProcAddress(ebacktrace, "SetProperName");

		if (SetTechnicalName)
			SetTechnicalName(APPLICATION_SHORTNAME);

		if (SetProperName)
			SetProperName(APPLICATION_NAME);

		colorprintf(C_BOLD C_RED "-=Debugging Enabled=- If you get a crash, send crash.log to Striker!\n" C_RESET);
	}
#endif

	// --- URL HANDLER ---
	HKEY hKey;
	if (RegCreateKeyEx(HKEY_CURRENT_USER, "SOFTWARE\\Classes\\netlaunch", 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, NULL) == ERROR_SUCCESS)
	{
		const char* value = "URL:Netlauncher Protocol";
		RegSetValueEx(hKey, "", 0, REG_SZ, (LPBYTE)value, strlen(value) + 1);

		value = "";
		RegSetValueEx(hKey, "URL Protocol", 0, REG_SZ, (LPBYTE)value, 0);
	}

	if (RegCreateKeyEx(HKEY_CURRENT_USER, "SOFTWARE\\Classes\\netlaunch\\shell\\open\\command", 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, NULL) == ERROR_SUCCESS)
	{
		char value[MAX_PATH];
		snprintf(value, sizeof(value), "%s\\NetLauncher.exe -url \"%1\"", path);
		RegSetValueEx(hKey, "", 0, REG_SZ, (LPBYTE)value, strlen(value) + 1);
	}
	// -------------------

	//atexit(killme);
	signal(SIGABRT, killme);
	signal(SIGINT, killme);
	signal(SIGTERM, killme);
#endif

	if (enet_initialize() != 0)
	{
		colorprintf(C_BOLD C_RED "An error occurred while initializing ENet.\n" C_RESET);
		return 1;
	}
	else
	{
		colorprintf(C_GREEN "ENet Initialized!\n" C_RESET);
	}

	lobby.modname[0] = 0;

	// Command Line Arguments
	for (int curarg = 0; curarg < argc; curarg++)
	{
		if (argv[curarg] != NULL)
		{
			if (strcasecmp(argv[curarg], "-url") == 0)
			{
				if (argv[curarg + 1] != NULL && strstr(argv[curarg + 1], "netlaunch://"))
				{
					colorprintf("Parsing URL - %s\n", argv[curarg + 1]);
					ip = (char*)malloc(strlen(argv[curarg + 1]));
					ip = extract_between(argv[curarg + 1], "netlaunch://", "/");
					if (ip != NULL && isValidIpAddress(ip))
					{
						colorprintf("IP is valid! - %s\n", ip);
						start_with_ip = true;
					}
				}
				else
				{
					colorprintf(C_BOLD C_RED "ERROR: Given an invalid URL! - %s\n", argv[curarg + 1]);
				}
			}
			
			#if 0 // Disabled Args
			if (strcasecmp(argv[curarg], "-lan") == 0)
			{
				lanmode = true;
			}
			#endif
		}
	}

	lobby.partySize = 1;

	char tempname[64];
	if (get_private_profile_string("Comm Setup", "PlayerName", "", tempname, sizeof(tempname), EDUKE32_CONFIG))
	{
		convertcolorcodes(clientname, tempname);
	}
	else
	{
		printf(EDUKE32_CONFIG " not found, using system username instead. Forgot to setup eduke32-oldmp?\n");
		DWORD size = sizeof(clientname);
		GetUserName(clientname, USERNAME_SIZE(size));
	}

	//fprintf(stderr, "\nUsername: " C_BOLD C_CYAN "%s\n" C_RESET, clientname);
	colorprintf("\nUsername: " C_BOLD C_CYAN "%s\n\n" C_RESET, clientname);

	if (start_with_ip && ip != NULL)
	{
		ConnectToHost(ip);
	}
	else
	{
		AskIfHostingOrJoining();
	}

	killme(0);
	return 0;
}

