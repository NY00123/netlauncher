#ifndef _CROSSPLAT_
#define _CROSSPLAT_

//#define UNICODE
//#define _UNICODE
#include "tinydir/tinydir.h"

#define C_BOLD		"\x1b[1m"
#define C_NOBOLD	"\x1b[22m"
#define C_BLACK		"\x1b[30m"
#define C_RED		"\x1b[31m"
#define C_GREEN		"\x1b[32m"
#define C_YELLOW	"\x1b[33m"
#define C_BLUE		"\x1b[34m"
#define C_MAGENTA	"\x1b[35m"
#define C_CYAN		"\x1b[36m"
#define C_WHITE		"\x1b[37m"
#define C_RESET		"\x1b[0m"

#ifndef _WIN32

#define Sleep(x) usleep((x)*1000)
#define SOCKET int
#define INVALID_SOCKET (-1)
#define SOCKET_ERROR (-1)
#define MAX_PATH PATH_MAX
#define closesocket close

#define SOCKADDR_IN sockaddr_in
#define SOCKADDR sockaddr

#define CLEAR_SCREEN system("clear")
#define TCHAR char
#define DWORD unsigned long

#define USERNAME_SIZE(x) x
#define GetUserName getlogin_r

#else

#define strcasecmp _stricmp
#define chdir _chdir
#define close closesocket
#define itoa _itoa
#define SHUT_RDWR SD_BOTH

#define CLEAR_SCREEN system("cls")

#define USERNAME_SIZE(x) &x

#endif

#endif
