======================================================================
THIS LEVEL IS NOT MADE BY OR SUPPORTED BY 3D REALMS.

Title                 : Mikko Sandt Dukematch 5: Dukematch 2000
Filename              : MSDM5
Release Date          : July 17
Author                : Mikko Sandt
E-mail                : sirpa.sandt@pp.inet.fi or jackotrades5@hotmail.com ( send comments or I won't make any more maps )
ICQ                   : 74485464
Web Page              : http://www.planetduke.com/msdn/ Huge Duke Nukem site
			http://www.dukeworld.com/duketournament/ My own add-on TC. Get your own copy
			I also have one page under construction. I'll let you know later at my sites.
Misc. Author Info     : Duke map and add-on king from Finland
Other Levels          : From MSSP1 to MSSP5 and from DM1 to MSDM5 ( and lot more )

Description           : Medieval/Hi-tech level which is much better than MSDM4. Obviously
			inspired by Quake3Arena. Lots of curves, fires and dark feeling.
			Map includes easy to use bots.bat file that lets you to play MSDM5
			level with bots.

			When I finished MSSP5 I decided that I will not make any more Duke
			maps but well here is one more. I decide again now that I will not
			make any more Duke maps:-)

			This map will be also included to "Duke Tournament Mission Pack: Dukematch 2000". More info from DT site.

Additional Credits To : 3DRealms: For great game that still rocks
			Id software: For Q3A which gave me some inspiration for this map
			Bob Averill: For inspired me to make a DM map when he released Bobdm1
			Airhawke: For Beta-test and complains to make MSDM5 bigger ( without him this map would have been double times smaller )

			I also wanna thank Britney Spears for great music that made me glad when I was bored so I got more strenght to make this map.
                          
			I also want to thank you for downloading and playing this map

======================================================================

* Play Information *

Episode and Level #    : User map
Single Player          : Yes with bots. To launch game double click bots.bat or type bots
DukeMatch 2-8 Player   : Yes. Excellent for seven players.
Cooperative 2-8 Player : No
Difficulty Settings    : Not implemented
Plutonium Pak Required : Yes
New Art                : No
New Music              : No
New Sound Effects      : No
New .CON Files         : No
Demos Replaced         : No

=====================================================================

* Construction *

Base                   : New level from scratch
Level Editor(s) Used   : BUILD
Art Editor(s) Used     : None
Construction Time      : About three weeks
Known Bugs/Problems    : None. If you find one then blame my Beta-testers :-)

* Where to get this MAP file *

File location          : MSDN: http://www.planetduke.com/msdn/
 			 Also from DT Mission pack ( not yet released )
			 Http://www.dukeworld.com/duketournament/

			 And perhaps some other Duke sites also

=====================================================================

*Important Information*

Installation           : Just unzip all files to your Duke directory. Then run setup and pick up the level.
			 To start level easier and with bots type or click bots.bat

Important Notes        : Place this level to your sites if you wish and feel free to send me 
			 your comments. Also include this text file with map and remember that
			 MSDM5 is made by me. If you wish to use some part of this level in your
			 map, then ask me first.

======================================================================

Go to http://duke-zone.tripod.com/Links for more awesome levels !
